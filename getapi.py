import pyodbc
from fastapi import FastAPI
from pydantic import BaseModel
from typing import List, Dict
from datetime import date

# configuration
server = "SIRIVIMOL_TH\SQLEXPRESS"
database = 'testdb'
username = 'mylogin'
password = '1234'

# connect database
cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER=' +
                      server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
cursor = cnxn.cursor()

app = FastAPI()


class Units(BaseModel):
    default_contract: str
    unit_id: str
    unit_number: str
    house_number: str
    owner_modify_date: str
    owner_create_date: str


class Ownerships(BaseModel):
    company_id: str
    project_id: str
 #   units: Units


class Customer(BaseModel):
    customer_id: str
    customer_code: str
    firstname_en: str
    lastname_en: str
    firstname_th: str
    lastname_th: str
    nationality: str
    mobile: str
    email: str
    modify_date: str
    create_date: str
    ownerships: Ownerships


class GetData(BaseModel):
    message: str
    code: int
    data: Customer


# query api
cursor.execute(
    "SELECT TOP (1000) [customer_id] ,[customer_code] ,[firstname_en] ,[lastname_en] ,[firstname_th] ,[lastname_th] ,"
    "[nationality] ,[mobile] ,[email] ,[modify_date] ,[create_date]  FROM [testdb].[dbo].[customer];")
customer_row = cursor.fetchall()

cursor.execute("SELECT TOP (1000) [ownerships_id] ,[company_id] ,[project_id] ,[customer_id] FROM [testdb].[dbo].["
               "ownerships]")

ownership_row = cursor.fetchall()

cursor.execute("SELECT TOP (1000) [default_contract] ,[unit_id] ,[unit_number] ,[house_number] ,"
               "[owner_modify_date] ,[owner_create_date] ,[ownerships_id]  FROM [testdb].[dbo].[units]")

unit_row = cursor.fetchall()

for row in customer_row:
    for row2 in ownership_row:
        for row3 in unit_row:
            unit_data: Units = Units(default_contract=row3[0], unit_id=row3[1], unit_number=row3[2], house_number=row3[0], owner_modify_date=row3[0], owner_create_date=row3[0])
        ownership_data = Ownerships(company_id=row2[1], project_id=row2[2])
        customer_data: Customer = Customer(customer_id=row[0], customer_code=row[1], firstname_en=row[2],
                                           lastname_en=row[3], lastname_th=row[5], firstname_th=row[4],
                                           nationality=row[6], mobile=row[7], email=row[8], modify_date=row[9],
                                           create_date=row[10], ownerships=ownership_data)

#    print(row)

# def process_items(items: List[str]):
#     for item in items:
#    print(item)

response_data: GetData = GetData(message="Success", code=200, data=customer_data)


# create GET api
@app.get("/")
async def read_root():
    return response_data


@app.get("/items/{item_id}")
async def read_item(item_id: int, q: str = None):
    return {"item_id": item_id, "q": q}

# run ด้วยคำสั่ง uvicorn getapi:app --reload
# http://127.0.0.1:8000/docs