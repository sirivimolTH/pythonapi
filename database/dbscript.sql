USE [master]
GO
/****** Object:  Database [testdb]    Script Date: 1/23/2020 10:04:57 AM ******/
CREATE DATABASE [testdb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'testdb', FILENAME = N'd:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\testdb.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'testdb_log', FILENAME = N'd:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\testdb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [testdb] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [testdb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [testdb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [testdb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [testdb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [testdb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [testdb] SET ARITHABORT OFF 
GO
ALTER DATABASE [testdb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [testdb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [testdb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [testdb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [testdb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [testdb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [testdb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [testdb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [testdb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [testdb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [testdb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [testdb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [testdb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [testdb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [testdb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [testdb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [testdb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [testdb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [testdb] SET  MULTI_USER 
GO
ALTER DATABASE [testdb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [testdb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [testdb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [testdb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [testdb] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [testdb] SET QUERY_STORE = OFF
GO
USE [testdb]
GO
/****** Object:  User [mylogin]    Script Date: 1/23/2020 10:04:57 AM ******/
CREATE USER [mylogin] FOR LOGIN [mylogin] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [mylogin]
GO
/****** Object:  Table [dbo].[customer]    Script Date: 1/23/2020 10:04:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customer](
	[customer_id] [nvarchar](255) NULL,
	[customer_code] [nvarchar](255) NULL,
	[firstname_en] [nvarchar](255) NULL,
	[lastname_en] [nvarchar](255) NULL,
	[firstname_th] [nvarchar](255) NULL,
	[lastname_th] [nvarchar](255) NULL,
	[nationality] [nvarchar](255) NULL,
	[mobile] [nvarchar](255) NULL,
	[email] [nvarchar](255) NULL,
	[modify_date] [nvarchar](255) NULL,
	[create_date] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ownerships]    Script Date: 1/23/2020 10:04:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ownerships](
	[ownerships_id] [int] IDENTITY(1,1) NOT NULL,
	[company_id] [nvarchar](255) NULL,
	[project_id] [nvarchar](255) NULL,
	[customer_id] [nvarchar](50) NULL,
 CONSTRAINT [PK_ownerships] PRIMARY KEY CLUSTERED 
(
	[ownerships_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[units]    Script Date: 1/23/2020 10:04:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[units](
	[default_contract] [bit] NULL,
	[unit_id] [nvarchar](255) NULL,
	[unit_number] [nvarchar](255) NULL,
	[house_number] [nvarchar](255) NULL,
	[owner_modify_date] [datetime] NULL,
	[owner_create_date] [datetime] NULL,
	[ownerships_id] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[units] ADD  CONSTRAINT [DF_units_default_contract]  DEFAULT ((0)) FOR [default_contract]
GO
ALTER TABLE [dbo].[units] ADD  CONSTRAINT [DF_units_owner_modify_date]  DEFAULT (getdate()) FOR [owner_modify_date]
GO
ALTER TABLE [dbo].[units] ADD  CONSTRAINT [DF_units_owner_create_date]  DEFAULT (getdate()) FOR [owner_create_date]
GO
USE [master]
GO
ALTER DATABASE [testdb] SET  READ_WRITE 
GO
